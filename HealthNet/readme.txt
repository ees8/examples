Language: Python (Using the Django web framework)

What it is: A website that's designed for a hospital to use to interface patient doctors and nurses.
Has various functions that were designed around use-cases given by a simulated customer. 
A good example is the website allowing registered patients to make appointments that doctors can view and approve

Reason for including: This project was designed by a team of 5 that I lead as the team coordinator and was received exceptionally by the professor.
I feel it is important to include as it highlighted my abilities outside of pure technical skill, 
and the final product is a testament to my ability to lead and work in a team in general.
In addition to the code there is documentation included in the directory that shows an
understanding of software engineering practices. 

Since this project was developd by a group of people it should be clear that I do not take credit for all the code or work in this project and instead
of providing it to show case the source code; I would like to bring attention to the functionality of the 
website that was brought about by the collabaration of the group.

How to use: The readme.pdf in the healthnet_BKZ directory contains detailed instrusction on use (for Windows).

To run the website a potential client needs to have Python 2.7 installed.
The client is platform independent but running the batch file, which is the easiest way to start it,
is the method described in the read me.