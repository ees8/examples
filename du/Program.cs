﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace du
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var done = false;
            //Stops the program and prints usage if arguments are not correct
            if (args.Length == 2)
            {
                if (!(args[0] == "-s" || args[0] == "-p" || args[0] == "-b")
                    || !(Directory.Exists(args[1])))
                    done = true;
                if (!(Directory.Exists(args[1])))
                    System.Console.WriteLine("\nError: Directory does not exist.\n");
            }
            else done = true;
            if (done)
            {

                System.Console.WriteLine("Usage: du [-s] [-p] [-b] <path>\n" +
                "Summarize disk usage of the set of FILEs, recursively for directories.\n\n" +

                "You MUST specify one of the parameters, -s, -p, or -b\n" +
                "-s  Run in single threaded mode\n" +
                "-p  Run in parallel mode(uses all available processors)\n" +
                "-b  Run in both single threaded and parallel mode\n" +
                "    Runs sequential followed by parallel mode.");
                Environment.Exit(0);
            }
            var mode = args[0];
            var rootDir = args[1];

            //Creates initial traversal object and then starts its search
            DateTime start;
            DateTime end;
            double pTime = 0;
            double sTime = 0;
            DiskReader pDiskR = null;
            DiskReader sDiskR = null;

            if (mode == "-p" || mode == "-b")
            {
                pDiskR = new DiskReader(rootDir);
                start = DateTime.Now;
                pDiskR.beginSearch('p');
                end = DateTime.Now;
                pTime = ((end - start).TotalSeconds);
            }
            if (mode == "-s" || mode == "-b")
            {
                sDiskR = new DiskReader(rootDir);
                start = DateTime.Now;
                sDiskR.beginSearch('s');
                end = DateTime.Now;
                sTime = ((end - start).TotalSeconds);
            }
            //Output at end so any non-fatal error messages won't interfere
            Console.WriteLine("\nDirectory  '{0}':\n", rootDir);
            if (mode == "-p" || mode == "-b")
            {
                printRunInfo(pDiskR, pTime, 'p', rootDir);
            }
            if (mode == "-s" || mode == "-b")
            {
                printRunInfo(sDiskR, sTime, 's', rootDir);
            }
        }

        // Gets passed info from main and then prints the standard
        // result message for either mode
        public static void printRunInfo (DiskReader diskR, double time, char modeCh, string dir)
        {
            string mode;
            if (modeCh == 'p')
                mode = "Parallel";
            else
                mode = "Sequential";

            Console.WriteLine("{0} Calculated in: {1}", mode, time);
            var folders = diskR.TotalFolders.ToString("N0");
            var files = diskR.TotalFiles.ToString("N0");
            var bytes = diskR.TotalBytes.ToString("N0");
            Console.WriteLine("{0} folders, {1} files, {2} bytes\n", folders, files, bytes);
        }
    
    }
    
    
    // Class that reads searches the disk and stores data it finds.
    // Exact function depend on specified mode.
    public class DiskReader
    {
        private DirectoryInfo rootdi;

        private long totalBytes = 0;
        public long TotalBytes
        {
            get { return totalBytes; }
            set { totalBytes = value; }
        }

        private long totalFolders = 0;
        public long TotalFolders
        {
            get { return totalFolders; }
            set { totalFolders = value; }
        }

        private long totalFiles = 0;
        public long TotalFiles
        {
            get { return totalFiles; }
            set { totalFiles = value; }
        }

        //Constructs the object by getting the Directory info
        //from the string path.
        public DiskReader(string path)
        {
            rootdi = new DirectoryInfo(path);
        }

        // Public method that main can access to launch a search
        // takes a character to define mode then launches the 
        // corresponding method internally.
        public void beginSearch (char modeCh)
        {
            if (modeCh == 's')
                singleSearch(rootdi);
            else if (modeCh == 'p')
                parallelSearch(rootdi);
        }

        // The sequential search method.
        // Runs via recursion, running the method again
        // to search each subdirectory it finds in the directory.
        private void singleSearch(DirectoryInfo mDir)
        {

                //Counts and totals size of files 
                var fiList = mDir.GetFiles();
                //Checks if there are any files in the directory
                if (!(fiList == null) || !(fiList.Length == 0))
                {

                    foreach (FileInfo fi in fiList)
                    {
                        totalFiles++;
                        totalBytes += fi.Length;
                    }
                }

                //Finds and recursivley traverses subdirectories
                var sDirList = mDir.GetDirectories();
                //Checks if there are any folders in the directory
                if (!(sDirList == null) || !(sDirList.Length == 0))
                {

                    foreach (DirectoryInfo sDir in sDirList)
                    {
                    try
                    {
                        totalFolders++;
                        singleSearch(sDir);
                    }
                    // Catch in case the DiskReader trys to access a file it the program
                    // doesn't have permission to.
                    catch (UnauthorizedAccessException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Folder skipped, run again as administrater for accurate result.");
                    }
                }

            }
        }

        // The parallel search method
        // Runs via recursion, launching a method on a new
        // thread each time it finds a subdirectory.
        private void parallelSearch(DirectoryInfo mDir)
        {

                //Counts and totals size of files 
                var fiList = mDir.GetFiles();
                if (!(fiList == null) || !(fiList.Length == 0))
                {
                // Files are also read in parallel after finding it resulted in an
                // average time increase.
                    Parallel.ForEach(fiList, (fi) =>
                   {
                       Interlocked.Increment(ref totalFiles);
                       Interlocked.Add(ref totalBytes, fi.Length);
                   });
                }

                //Finds and recursivley traverses subdirectories
                var sDirList = mDir.GetDirectories();
                if (!(sDirList == null) || !(sDirList.Length == 0))
                {

                    Parallel.ForEach(sDirList, (sDir) =>
                   { 
                       try
                       {

                        Interlocked.Increment(ref totalFolders);
                        parallelSearch(sDir);

                       }
                       catch (UnauthorizedAccessException e)
                       {
                           Console.WriteLine(e.Message);
                           Console.WriteLine("Folder skipped, run again as administrater for accurate result.");
                       }
                   });
                    
                }

        }

    }
}
