Language: C# (.NET)

What it is: A more basic version of the GNU du tool written with a single-threaded and multi-threaded mode. Simplification is in the fact that
it does not print each directory and instead prints file count, folder count, and the speed of the run.

Reason for including: Shows the use of multiple threads as well as the ability to write in C# with the .NET framework

How to use: 
Usage: du [-s] [-p] [-b] <path>
Summarize disk usage of the set of FILEs, recursively for dirs

You MUST specify one of the parameters, -s, -p, or -b
-s  Run in single threaded mode
-p  Run in parallel mode(uses all available processors)
-b  Run in both single threaded and parallel mode
    Runs sequential followed by parallel mode.

@TODO Include Executable for Windows 10