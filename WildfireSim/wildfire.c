/*
 * wildfire.c
 *
 *
 * Author: ees8346, Evan Scholl
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>

#define EMPTY 0
#define TREE 1
#define BURNING 2
#define BURNT 3

static int size;
static int mode;
static int totalChange;
static double probab;
static double treeDen;
static double proportionBurn;

//Structure for storing properties of a place on the grid
//so only one array can be used.
struct Space {
	int* spot;
	int adjacentBurn;
	int adjacentTree;
} ;

/*
 * Multi-purpose function that both initializes
 * and updates the burning trees.
 * @param type - purpose used for: 1 = trees; 2 = burning trees;
 * 3 = burned trees
 * @param prob - probability of whatever the type is
 * @param grid - global grid
 * return - number of changes made
 */
static int update (int type, double prob, int grid[size][size]){
	int change=0;
	srand(time(NULL));
	double r;
	for (int row=0; row<size; row++)
	{
		for (int col=0; col<size; col++)
		{

			r = (double)rand() / (double)RAND_MAX;
			if ((r<prob && ((type==BURNT && grid[row][col]==2) ||
					type==TREE || (type==BURNING && grid[row][col]==TREE)))){
				grid[row][col] = type;
				change++;
			}
		}
	}
	return change;
}

/*
 * Prints a usage method if file was ran incorrectly.
 * @param - accompanying message to be printed
 */
static void usage(char* msg) {
	fprintf(stderr, "%s", msg);
	fprintf(stderr, "usage: wildfire [-pN] size probability treeDensity proportionBurning\n"
			"The -pN option tells the simulation to print N cycles and stop.\n"
			"The probability is the probability a tree will catch fire.\n");
}

/*
 * Checks a tree spot to see if there is a
 * burning tree adjacent to it.
 * @param grid - the global grid
 * @param row - the row the spot being checked is in
 * @param col - the column the spot being checked is in
 * returns number of adjacent trees
 */
int checkAdj (int type, int grid[size][size], int row, int col){
	int c = 0;
	if (col !=0 && grid[row][col-1]==type)
		c++;
	if (col !=size-1 && grid[row][col+1]==type)
		c++;
	if ((col !=0 && row != size-1) && grid[row+1][col-1]==type)
		c++;
	if ((col !=0 && row != 0) && grid[row-1][col-1]==type)
		c++;
	if (row !=0 && grid[row-1][col]==type)
		c++;
	if (row !=size-1 && grid[row+1][col]==type)
		c++;
	if ((row !=0 && col !=size-1) && grid[row-1][col+1]==type)
		c++;
	if ((row !=size-1 && col !=size-1) && grid[row+1][col+1]==type)
		c++;
	return c;
}

/*
 * Applies the spreading fire
 * @param trees - array of spots with trees in them
 * @param count - size of the trees parameter
 */
static int applySpread (struct Space trees[size*size], int count) {
	int change=0;
	srand(time(NULL));
	double r;
	for (int i=0; i<count; i++){
		r = (double)rand() / (double)RAND_MAX;
		if ((double) trees[i].adjacentBurn/trees[i].adjacentTree > 0.25 && r<probab){
			*trees[i].spot=BURNING;
			change++;
		}
	}
	return change;
}

/*
 * Returns a character for corresponding to the
 * number in a spot for the output functions.
 * @param type - number in spot
 * @return char corresponding to the number
 */
static char getCharFor(int type){
	switch(type) {
	case EMPTY :
		return ' ';
		break;
	case TREE :
		return 'Y';
		break;
	case BURNING :
		return '*';
		break;
	case BURNT :
		return '_';
		break;
	default :
		break;
	}
	return 'X';
}


/*
 * Prints the grid using the cursor controlled method
 * @param - the global grid
 * @param c - current cycle
 * @param change - number of changes from last cycle
 */
static void printGrid(int grid[size][size], int c,  int change){
	int row=0;
	int col=0;
	clear();
	set_cur_pos(0, 0);
	for (row=0; row<size; row++)
		{
		for (col=0; col<size; col++){
			set_cur_pos(row,col);
			put(getCharFor(grid[row][col]));
		}
		}
	char msg[] = "cycle %i, size %i, probability %.2f, density %.2f, proportion %.2f, changes %i";
	char str[200];
	sprintf(str, msg, c, size, probab, treeDen, proportionBurn, change);
	set_cur_pos(size+1, 0);
	for (int i = 0; i < strlen(str);i++)
		put(str[i]);
}

static void printfGrid(int grid[size][size], int c,  int change){
	int row, col;
	for (row=0; row<size; row++)
		{
			for (col=0; col<size; col++){
				putchar(getCharFor(grid[row][col]));
			}
			printf("\n");
		}
	printf("cycle %i, size %i, probability %.2f, density %.2f, proportion %.2f, changes %i\n",
		c, size, probab, treeDen, proportionBurn, change);
}

int main (int argc, char * argv[] ){
	mode = 0;
	int maxC = 0;
	int opt;
	while ( (opt = getopt( argc, argv, "p:") ) != -1 ) {
		switch ( opt ) {
		case 'p':
			mode = 1;
			maxC = atoi(optarg);
			if (maxC<0){
				usage("The -pN option was negative.\n");
				return 0;
			}
			break;
		case '?':
			usage("The -pN option was invalid.\n");
			return 0;
		}
	}
	//Accept arguments and check for errors in them
	size = atoi(argv[optind]);
	if (size<5 || size>40){
		usage("The size (X) must be an integer in [5...40].\n");
		return 0;
	}
	const int probability = atoi(argv[optind+1]);
	if (probability<0 || probability>100){
		usage("The probability (X) must be an integer in [0...100].\n");
		return 0;
	}
	probab = (double) probability/100;
	const int treeDensity = atoi(argv[optind+2]);
	if (treeDensity<0 || treeDensity>100){
		usage("The density (X) must be an integer in [0...100].\n");
		return 0;
	}
	treeDen = (double) treeDensity/100;
	const int propBurning = atoi(argv[optind+3]);
	if (propBurning<0 || propBurning>100){
			usage("The proportion (X) must be an integer in [0...100].\n");
			return 0;
		}
	proportionBurn = (double) propBurning/100;

	//initialize grid
	int grid[size][size];
	memset(grid, EMPTY, size*size*sizeof(int) );
	update (TREE, (double) treeDensity/100, grid);
	update (BURNING, (double) propBurning/100, grid);

	//Simulation cycle
	int treeCount;
	int burnCount;
	int run = 1;
	struct Space trees[size*size];
	int adjCount;
	int changes = 0;
	int cycle = 0;
	if(mode == 0){
	printGrid(grid, cycle, changes);
	usleep(520000);
	}
	else printfGrid(grid, cycle, changes);
	while(run == 1 && !(mode == 1 && cycle == maxC))
	{
		//iterate through, making a list of trees and whether
		//they have a burning tree next to them.
		changes=0;
		treeCount = 0;
		burnCount = 0;
		for (int row=0; row<size; row++)
			{
				for (int col=0; col<size; col++)
				{
					if (grid[row][col] == TREE)
					{
						trees[treeCount].spot = &grid[row][col];
						adjCount=checkAdj(BURNING, grid, row, col);
						trees[treeCount].adjacentBurn=adjCount;
						adjCount=checkAdj(TREE, grid, row, col);
						trees[treeCount].adjacentTree=adjCount;
						treeCount++;
					}
					else if (grid[row][col] == BURNING)
					{
						burnCount++;
					}
				}
			}
		//Apply the next step if there are burning trees
		if (burnCount != 0)
		{
			changes += update(BURNT, 0.80, grid);
			changes += applySpread(trees, treeCount);
			cycle++;
		}
		else{
			run = 0;
		}
		totalChange+=changes;
		if(changes != 0){
		    if(mode == 0){
		    printGrid(grid, cycle, changes);
		    usleep(520000);
		    }
		    else printfGrid(grid, cycle, changes);
		}
	}
	if(mode == 0){
		set_cur_pos(size+2,0);
		char msg[] ="fires are out after %i cumulative changes.";
		char str[200];
		sprintf(str, msg, totalChange);
		for (int i = 0; i < strlen(str);i++)
		put(str[i]);
	}
	else{
		printf("fires are out after %i cumulative changes.", totalChange);
	}
	printf("\n");
}

