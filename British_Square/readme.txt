Language: Java 

What it is: A program that simulates the board game "British Square". Allows for Player Vs. Player, Player Vs. Computer and Computer Vs. Computer gameplay with multiple options for how to computer plays

Reason for including: A larger program that takes advantage of object oriented code and shows off a functional application/

How to use: Arguements are:

"Usage: java BritishSquare player-X player-O [brdSize]
where player-X and player-O are one of: human, bad, good, random
[brdSize] is optional, if provided it must be in the range from: 3 to 7 "
                
Other than that the player is prompted for commands.