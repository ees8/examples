/* 
 * BritishSquare.java 
 * 
 * Version: 
 *     $Id: BritishSquare.java,v 1.12 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: BritishSquare.java,v $
 *     Revision 1.12  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.11  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */

/**
 * The main class that runs through the game process
 * @author Evan
 *
 */
public class BritishSquare {
	
	public static void main(String[] args){
			try{
				//creates board
			Board board = new Board(args);
				//starts game
			while(!board.isGameOver()){
				//check if last player passed to potentially skip printing
				if(!board.isPassed()){
				board.printBoard();
				System.out.println();
				}
				else board.unpass();
				//Player 1 moves
				System.out.println(board.getPlayer1().getName()+" player X moving...");
				System.out.println(board.move('X',board.getPlayer1().getNextMove(board.getLegals('X'))));
				//checks to see if moves remain, and if not ends game
				if (board.isGameOver())
					break;
				if(!board.isPassed()){
				board.printBoard();
				System.out.println();
				}
				else board.unpass();
				//Player 2 moves
				System.out.println(board.getPlayer2().getName()+" player O moving...");
				System.out.println(board.move('O',board.getPlayer2().getNextMove(board.getLegals('O'))));
				board.nextTurn();
			}
			board.printBoard();
			int Xscore=board.getXscore();
			int Oscore=board.getOscore();
			System.out.println("\nNo more legal moves available, the game is over\nFinal Score: X = "+Xscore+" : O = "+Oscore);
			if(Oscore==Xscore)
				System.out.println("Its a tie, no one wins");
			else{
			char winner = Xscore>Oscore ? 'X' : 'O';
			System.out.println("Player "+winner+" won");
			}
			}catch (IllegalArgumentException e){
				System.err.println(e.getMessage());
			}
			
		}
	}
