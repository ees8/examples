/* 
 * humanPlayer.java 
 * 
 * Version: 
 *     $Id: humanPlayer.java,v 1.5 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: humanPlayer.java,v $
 *     Revision 1.5  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.4  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;
/**
 * Defines a player that moves based on human input
 * @author Evan
 *
 */
public class humanPlayer extends Player {
	private char player;
	
	public humanPlayer(char player) {
		super("human");
		this.player=player;
	}
	
	/**
	 * Prompts the user for a move, checks if it's legal
	 * and returns it if it is.
	 * 
	 * @param legalSpaces A list of possible moves
	 * @return Chosen move
	 */
	@Override
	public int getNextMove(ArrayList<Integer> legalSpaces) {
		if (legalSpaces.isEmpty())
			return -1;
		Integer move;
		while(true){
			System.out.print("Player "+player+": Enter the location to place your piece (-1 to quit): ");
			move=Board.getInput();
			if (move==-1){
				System.out.println(player+" quits the game");
				//I'm sorry
				System.exit(0);
			}
			else if (!(legalSpaces.contains(move)))
				System.out.println("invalid location: "+move);
			else return move; 
		}
	}

}
