/* 
 * goodPlayer.java 
 * 
 * Version: 
 *     $Id: goodPlayer.java,v 1.3 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: goodPlayer.java,v $
 *     Revision 1.3  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.2  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Evan
 *
 */
public class goodPlayer extends Player {
	private Board board;
	private char player;
	/**
	 * Gets additional values needed for the function
	 * 
	 * @param board Game board
	 * @param player Symbol of the playing character
	 */
	public goodPlayer(Board board,char player) {
		super("good");
		this.board=board;
		this.player=player;
	}

	/** 
	 * Gets the value of each move by adding the values of each
	 * adjacent square for every possible move. 1 for blocking
	 * a square the opponent is already blocking
	 * 2 for one that is completely free.
	 * It then chooses the move with most value. 
	 * If their is a tie, it chooses one closest to the middle.
	 * 
	 *@param legalSpaces  List of possible moves
	 */
	@Override
	int getNextMove(ArrayList<Integer> legalSpaces) {
		if (legalSpaces.isEmpty())
			return -1;
		int[] spaceValue=new int[legalSpaces.size()];
		for (int i=0;i<legalSpaces.size();i++){
			BoardSquare[] adjacents=board.getAdjacents(legalSpaces.get(i));
			int sum=0;
			for (int k=0;k<4;k++){
				if (adjacents[k]!=null)
				{
				if (adjacents[k].getBlockedBy()=='n')
					sum+=2;
				else if(adjacents[k].getBlockedBy()!=player)
					sum++;
				}
			}
			spaceValue[i]=sum;
		}
		int bestMove=-1;
		int max=0;
		for (int j=0;j<legalSpaces.size();j++){
			if ((spaceValue[j]==max&&legalSpaces.get(j)-board.getBoardSize()/2<bestMove-board.getBoardSize()/2)
					||spaceValue[j]>max){
				bestMove=legalSpaces.get(j);
				max=spaceValue[j];
			}
		}
		return bestMove;
	}

}
