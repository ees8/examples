/* 
 * random.java 
 * 
 * Version: 
 *     $Id: randomPlayer.java,v 1.3 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: randomPlayer.java,v $
 *     Revision 1.3  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.2  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;


/**
 * Defines a player that randomly chooses it's next move
 * @author Evan
 *
 */
public class randomPlayer extends Player {

	public randomPlayer() {
		super("random");
	}

	/** 
	 * Chooses it's next move randomly from the list of
	 * legal moves
	 * 
	 * @param legalSpaces A list of possible moves
	 * @return Chosen move
	 */
	@Override
	int getNextMove(ArrayList<Integer> legalSpaces) {
		if (legalSpaces.isEmpty())
			return -1;
		return legalSpaces.get((int)(Math.random()*legalSpaces.size()));
	}

}
