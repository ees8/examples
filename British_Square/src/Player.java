/* 
 * Player.java 
 * 
 * Version: 
 *     $Id: Player.java,v 1.3 2015/10/08 02:48:21 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: Player.java,v $
 *     Revision 1.3  2015/10/08 02:48:21  ees8346
 *     Final
 *
 *     Revision 1.2  2015/10/08 02:13:58  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;

/**
 * Abstract player class to allow polymorphism
 * @author Evan
 *
 */
public abstract class Player {
	private String name;
	
	/**
	 * Constructor, gives the player a name corresponding to it's type
	 * so we can use it for output
	 * @param name Name of the type of the player
	 */
	public Player(String name){
    this.name=name;
	}
	
	abstract int getNextMove(ArrayList<Integer> legalSpaces);
	
	/**
	 * Accessor for name
	 * 
	 * @return name
	 */
	public String getName(){
		return name;
	}

}
