/* 
 * Board.java 
 * 
 * Version: 
 *     $Id: Board.java,v 1.17 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: Board.java,v $
 *     Revision 1.17  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.16  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class that defines the board and manages the game
 * @author Evan
 *
 */
public class Board {
	private static Scanner keyboardInput = new Scanner(System.in);
	private Player player1;
	private Player player2;
	private int rowSize;
	private int boardSize;
	private boolean gameOver;
	private BoardSquare[] spaces;
	private boolean firstTurn;
	private int Xscore;
	private int Oscore;
	private boolean passed=false;
	
	/**
	 * Constructor for board. Sets up the game and players.
	 * 
	 * @param args arguments passed from main
	 */
	public Board(String[] args) throws IllegalArgumentException {
		IllegalArgumentException e=new IllegalArgumentException("Usage: java BritishSquare player-X player-O [brdSize]"
				+ "\nwhere player-X and player-O are one of: human, bad, good, random"
				+ "\n[brdSize] is optional, if provided it must be in the range from: 3 to 7 ");
		if (args.length==0)
			throw e;
		gameOver=false;
		//adds players
		for (int i=0;i<=1;i++){
			String entry=args[i];
			switch (entry){
			case "human":
				if (i==0)
					player1=new humanPlayer('X');
				else player2 = new humanPlayer('O');
				break;
			case "bad":
				if (i==0)
					player1=new badPlayer();
				else player2 = new badPlayer();
				break;
			case "random":
				if (i==0)
					player1=new randomPlayer();
				else player2 = new randomPlayer();
				break;
			case "good":
				if (i==0)
					player1=new goodPlayer(this,'X');
				else player2 = new goodPlayer(this,'O');
				break;
			default:
				throw e;
			}
		}
		//makes board
		if (args.length>2)
		rowSize=Integer.parseInt(args[2]);
		else
		rowSize=5;
		if (rowSize<3||rowSize>7)
			throw e;
		boardSize=rowSize*rowSize;
		spaces= new BoardSquare[boardSize];
		for (int k=0;k<boardSize;k++)
			spaces[k]=new BoardSquare();
		firstTurn=true;
	}
		
	public ArrayList<Integer> getLegals(char player){
		ArrayList<Integer> legalSpaces= new ArrayList<Integer>();
		for(int i=0;i<boardSize;i++){
			if(spaces[i].getBlockedBy()==player||spaces[i].getBlockedBy()=='n'&&!(boardSize%2==1&&firstTurn&&i==boardSize/2))
				legalSpaces.add(i);
		}
		return legalSpaces;
	}
	
	/**
	 * Executes a move, placing a piece at the passed index and blocking off the adjacent spaces 
	 * to the other player
	 * 
	 * @param player Symbol of moving player
	 * @param moveTo Index player is moving to
	 */
	public String move(char player, int moveTo){
		if (moveTo==-1){
			passed=true;
			return ""+player+" has no more moves and must skip turn.";
		}
		else{
			if (player=='X')
				Xscore++;
			else
				Oscore++;
			spaces[moveTo].place(player);
			spaces[moveTo].changeAvailable('b');
			BoardSquare[] changes=getAdjacents(moveTo);
			if (changes[0]!=null)
				spaces[moveTo-1].changeAvailable(player);
			if (changes[1]!=null)
				spaces[moveTo+1].changeAvailable(player);
			if (changes[2]!=null)
				spaces[moveTo-rowSize].changeAvailable(player);
			if (changes[3]!=null)
				spaces[moveTo+rowSize].changeAvailable(player);
		}
		if(getLegals('X').isEmpty()&&getLegals('O').isEmpty())
			gameOver=true;
		return "Player places an "+player+" piece at location: "+moveTo;
		
	}
	/**
	 * Gets the index of every valid adjacent square to the location
	 * 
	 * @param loc Square to get adjacents of
	 */
	public BoardSquare[] getAdjacents(int loc){
			BoardSquare[] adjacents=new BoardSquare[4];
			
			if (loc%rowSize!=0)
				adjacents[0]=spaces[loc-1];
			else adjacents[0]=null;
			if ((loc+1)%rowSize!=0||loc==0)
				adjacents[1]=spaces[loc+1];
			else adjacents[1]=null;
			if (loc-rowSize>=0)
				adjacents[2]=spaces[loc-rowSize];
			else adjacents[2]=null;
			if (loc+rowSize<boardSize)
				adjacents[3]=spaces[loc+rowSize];
			else adjacents[3]=null;
			return adjacents;
	}
	
	/**
	 * Sets firstTurn to false, indicating at least one turn
	 * has passed so the middle can be unblocked
	 */
	public void nextTurn(){
		firstTurn=false;
	}
	
	/**
	*Prints out the board to standard out.
	*/
	public void printBoard(){
		char piece = 'n';
		for (int i=0;i<rowSize;i++){
			System.out.println();
			for (int l=0;l<rowSize;l++){
			System.out.print("+---");
			}
			System.out.print("+");
			for (int j=1;j<=2;j++){
				System.out.print("\n|");
				for (int k=0;k<rowSize;k++){
					piece=spaces[(rowSize*i)+k].getPiece();
					if(piece=='n')
						if (j==1)
							System.out.print("   |");
						else{
							if(rowSize*i+k<10)
								System.out.print((rowSize*i+k)+"  |");
							else
								System.out.print((rowSize*i+k)+" |");
						}
							else if(piece=='X')
								System.out.print("XXX|");
							else System.out.print("OOO|");
					}
				}
			}
		System.out.println();
		for (int l=0;l<rowSize;l++){
		System.out.print("+---");
		}
		System.out.println("+");
		}
	
	/**
	* Sets pass to true
	*/
	public void unpass(){
		passed=true;
	}
	
	/**
	* Accessor for Player1
	* 
	*  @return player1
	*/
	public Player getPlayer1(){
		return player1;
	}
	/**
	* Accessor for Player2
	* 
	* @return player2
	*/
	public Player getPlayer2(){
		return player2;
	}
	/**
	* Accessor for passed
	* 
	* @return passed
	*/
	public boolean isPassed(){
		return passed;
	}
	/**
	* Accessor for gameOver
	* 
	* @return gameOver
	*/
	public boolean isGameOver(){
		return gameOver;
	}
	/**
	* Accessor for Xscore
	* 
	* @return Xscore
	*/
	public int getXscore() {
		return Xscore;
	}
	/**
	* Accessor for Oscore
	* 
	* @return Oscore
	*/
	public int getOscore() {
		return Oscore;
	}
	/**
	* Accessor for boardSize
	* 
	* @return BoardSize
	*/
	public int getBoardSize(){
		return boardSize;
	}
	/**
	* Gets the next line integer entered into the scanner
	* 
	* @return The next integer in system.in
	*/
	public static int getInput(){
		return keyboardInput.nextInt();
	}
	
}
