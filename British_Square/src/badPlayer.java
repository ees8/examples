/* 
 * badPlayer.java 
 * 
 * Version: 
 *     $Id: badPlayer.java,v 1.3 2015/10/08 02:48:21 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: badPlayer.java,v $
 *     Revision 1.3  2015/10/08 02:48:21  ees8346
 *     Final
 *
 *     Revision 1.2  2015/10/08 02:13:58  ees8346
 *     Final
 * 
 */
import java.util.ArrayList;


/**
 * Defines a predictable, easy to beat player
 * @author Evan
 *
 */
public class badPlayer extends Player {


	public badPlayer() {
		super("bad");
	}

	/** 
	 * Tries to move to the first legal space,
	 * i.e. the most upper left one
	 * 
	 * @param legalSpaces A list of possible moves
	 * @return Chosen move
	 */
	@Override
	public int getNextMove(ArrayList<Integer> legalSpaces) {
		if (legalSpaces.isEmpty())
			return -1;
		return legalSpaces.get(0);
	}

}
