CSCI-140 Project 01: README
===============================
(please use the RETURN key to make multiple lines; don't assume autowrap.)

0. Author Information
---------------------

CS Username: 	ees8346		Name:  		Evan Scholl

-------------------
1. Problem Analysis
-------------------

Summarize the analysis work you did. 
What new information did it reveal?  How did this help you?
How much time did you spend on problem analysis?

	I basically just went over each element I thought I would need to solve the problem, and thought about how
	I should define them. This revealed the basic outline of what I needed to code, which helped me get started on the problem.
	How much time I spent exactly is unclear, I spent about half an hour before I started designing, thinking of how to start, 
	but I had also spent several days sporadically thinking about the best way to solve the problem. 

------------------
2a. General Design
------------------

Explain the design you developed to use and why. 
What are the major components? What connects to what?
How much time did you spend on design?

	I decided to use a collection of board squares to represent my board. To implement the core mechanic of blocking off squares,
	squares are marked with a player symbol or a symbol designating it's blocked to both players. This provides
	a fast way to find legal moves. To utilize this, a major component is the getLegals method. It iterates through the
	board adding every possible move to a player an ArrayList then returns it. Simple, but it's the most used method
	in my code. Every player classes use it to base their moves on, it's used to check if the game is over, and it's 
	also means that a player will automatically pass if the array is empty. The basic order for a move is a the main class
	tells the board that a player has to move, the board asks the player for it's move, the player asks getLegals to help select
	its move and returns it, then the board places a piece on location and blocks adjacent squares. This relys on the polymorphism of
	the different player classes, that all have the same method to get thier next move but different ways of getting them. I probably spent half
	an hour thinking on design outline before starting to code.

	

-----------------------------
2b. Design of the Good Player
-----------------------------

Explain the design behind your "good" player.  What was your overall
idea of a good move?

	My good player makes it's best move primarily based on how many squares it will block.
	It goes through each possible move and looks at the squares around it, adding a number
	to the consideration of that move for each square it could block. Blocking a free square
	is considered 2 while blocking one a player has blocked but not taken is worth 1
	adjacent squares that are already owned by the player or off the edge are worth 0. So a move
	that would block 2 previously unblocked spaces and 1 space adjacent to the others players piece
	would be "worth" 5. The player then goes through all the moves and chooses the one it decided has
	the highest value. If their is a tie it chooses the space closest to the center as there is a greater
	chance the spaces blocked there will have a higher value in the future(so not edge spots). 

-----------------------------
3. Implementation and Testing
-----------------------------

Describe your implementation efforts here; this is the coding and testing.

What did you put into code first?
How did you test it?
How well does the solution work? 
Does it completely solve the problem?
Is anything missing?
How could you do it differently?
How could you improve it?

How much total time would you estimate you worked on the project? 
If you had to do this again, what would you do differently?

	I started with the Board class that represented the board and made changes to it
	then branched out to making the players, and board square class as needed in the constructor.
	I only tested my solution after everything was working so I could compare them with vimdiff to the 
	example outs that were provided.
	The solution works very well. It has a good runtime, and isn't incapable of anything we were required to do.
	Good player also works well and is challenging enough to win against. 
	Nothing is missing that I am aware of.
	It doesn't have much effect on output but it would of been a bit easier to use a 2d array, I just decided against
	it. At the very least it would make the code a little bit less messy. I also feel I should of added a random element
	to good player so, if squares are of the same value and equally close to the center they won't always choose the same one.
	It bothers me that two goods will always tie for whatever reason.

	I spent about 9 or 10 hours on the project in total. If I did it again, like I said I would use a 2d array, but I would
	give myself more time to do it. 

----------------------
4. Development Process
----------------------

Describe your development process here; this is the overall process.

How much problem analysis did you do before your initial coding effort?
How much design work and thought did you do before your initial coding effort?
How many times did you have to go back to assess the problem?

What did you learn about software development?

	Like I said I pondered over the problem and possible solutions for several days
	so I had a pretty good idea of what was needed to solve the problem.
	I understood the problem pretty well so I did not spend a grand amount of time
	pre-planning. I designed the basic main tenants of the program, then as I coded
	the specifics were worked out. I had to go back a couple times so I could check certain
	rules or special cases that I forgotten to deal with.
	
	Mainly that you should try to get the overall function of the program working before you try and
	to specific or impressive things. I saved a lot of time by not worrying about good player until I 
	got everything else bug free. 

