/* 
 * filename.java 
 * 
 * Version: 
 *     $Id: BoardSquare.java,v 1.3 2015/10/08 02:48:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: BoardSquare.java,v $
 *     Revision 1.3  2015/10/08 02:48:22  ees8346
 *     Final
 *
 *     Revision 1.2  2015/10/08 02:13:59  ees8346
 *     Final
 * 
 */
/**
 * Defines a square on the board
 * @author Evan
 *
 */
public class BoardSquare {
	private char piece;
	private char blockedBy;
	
	/**
	 * Constructor. Sets the BoardSquare to have
	 * nothing on it
	 */
	public BoardSquare(){
		piece='n';
		blockedBy='n';
	}
	/**
	 * @param player
	 */
	public void changeAvailable(char player){
		if (blockedBy=='n')
			blockedBy=player;
		else if (!(blockedBy==player))
			blockedBy='b';
	}
	
	/**
	 * Places a player piece on the square
	 * 
	 * @param player Symbol of player placing piece
	 */
	public void place(char player) {
		piece=player;
	}
	
	/**
	 * Accessor for piece on square
	 * 
	 * @return Symbol of piece on square 
	 */
	public char getPiece() {
		return piece;
	}
	
	/**
	 * Gets the players, blocking the square from being
	 * moved on, if any
	 * 
	 * @return Symbol of a player, 'n' if none or 'b' if both
	 */
	public char getBlockedBy() {
		return blockedBy;
	}
	
}
