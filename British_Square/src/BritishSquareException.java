/**
 * 
 */

/**
 * @author Evan
 *
 */
public class BritishSquareException extends Exception {
	
	public BritishSquareException (String message){
		super (message);
	}

}
