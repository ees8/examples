/* 
 * filename.java 
 * 
 * Version: 
 *     $Id: InstrumentedArray.java,v 1.5 2015/09/20 00:35:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: InstrumentedArray.java,v $
 *     Revision 1.5  2015/09/20 00:35:22  ees8346
 *     Final
 *
 *     Revision 1.4  2015/09/20 00:21:55  ees8346
 *     Final
 *
 *     Revision 1.3  2015/09/19 23:54:40  ees8346
 *     Final
 * 
 */

/**
 * @author Evan
 *
 * Wraps around an array to count times accessed
 */
public class InstrumentedArray {
	private static int counter;
	private Integer[] array;
	
	/**
	 * Creates an instrumented array
	 * @param array the array the class holds
	 */
	public InstrumentedArray (Integer[] array){
		this.array=array;
		counter = 0;
	}
	/**
	 * Gets the value at the index
	 * @param i index value is at
	 * @return value
	 */
	public Integer get (int i) throws ArrayIndexOutOfBoundsException{
		try{
			if (i>=this.size()||i<0)
				throw new ArrayIndexOutOfBoundsException("Invalid index");
			int re = array[i] ;
			counter++;
			return re;
		}catch (ArrayIndexOutOfBoundsException e){
			return null;
		}
	}

	public void put (int i,Integer value){
		if (i<this.size()||i>=0)
			array[i]=value;
	}
	/**
	 * 
	 * @return Size of the array
	 */
	public int size(){
		return array.length;
	}
	/**
	 * Accessor for counter
	 * @return times array was accessed
	 */
	public int getTotalReads(){
		return counter;
	}
}
