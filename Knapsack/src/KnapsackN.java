import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

/* 
 * filename.java 
 * 
 * Version: 
 *     $Id: KnapsackN.java,v 1.7 2015/09/20 00:35:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: KnapsackN.java,v $
 *     Revision 1.7  2015/09/20 00:35:22  ees8346
 *     Final
 *
 *     Revision 1.6  2015/09/20 00:26:03  ees8346
 *     Final
 *
 *     Revision 1.5  2015/09/20 00:21:56  ees8346
 *     Final
 *
 *     Revision 1.4  2015/09/19 23:54:40  ees8346
 *     Final
 * 
 */
/**
 *
 * A solution to a restricted form of the "knapsack" or "subset sum"
 * problem where you are given a number N and asked if there is a
 * subset of size <em>exactly</em> N whose elements sum to the
 * desired value.
 *
 * @author Evan
 */
public class KnapsackN {

    /**
     * The array that will hold the entire set of numbers.
     */
    private InstrumentedArray a = null;

    /**
     * The array of the indices of the elements currently chosen
     * for summing.
     */
    private int[] choices = null;

    /**
     * The desired sum.
     */
    private int targetSum = 0;
    /**
     * Accesses to array
     */
    private static int accesses;
    /**
     * Read a sum and an array of integers. See if
     * a subset of the array elements sum to that value.
     *
     * @param args arg0 is size of subset to pick.
     */
    public static void main( String[] args ) {

/**************************************************************
YOUR CODE GOES HERE.
There are three error messages you must print when needed:

"Usage: java KnapsackN subset-size"
        -- if the number of arguments is wrong

"Subset size is non-positive."
        -- if the command line argument is 0 or less

        -- if the command line argument value (N) is greater than the
        -- number of elements read from standard input.
        -- (This can't be checked until the data has been read in.)

Here are some suggestions as to how to structure this method:

- Get the command line argument.
- Read the first data item as your sum.
- Read the rest of the data and put it in a list. We use a list
-   because we don't know the size of the data set ahead of time.
- Copy the data from the list into an array.
- Create an InstrumentedArray object containing the array.
- Create a Knapsack using the provided parameters.
- Search for a solution.

**************************************************************/
    	try{
    		if (args.length<=2)
    			throw new Exception("Usage: java KnapsackN subset-size");
    		/* Instructions contradicted itself on how to get the subset size and sum size, both saying it should be args0
    		 * So I just put args0 as subset size and args1 as sum.
    		 */
    		int N = Integer.parseInt(args[0]);
    		int K = Integer.parseInt(args[1]);
    		if (N<=0||N>args.length)
    			throw new Exception("Subset size is non-positive.");
    		List<Integer> list = new LinkedList<Integer>(); 
    		for (int k=2;k<args.length;k++){
    			list.add(Integer.parseInt(args[k]));
    		}
    		Integer[] data = new Integer[list.size()];
    		Iterator<Integer> grab = list.iterator();
    		int count=0;
    		while (grab.hasNext()){
    			data[count]=grab.next();
    			count++;
    		}
    		InstrumentedArray set = new InstrumentedArray(data);
    		KnapsackN sack = new KnapsackN(set,K,N);
    		if (sack.run())
    			System.err.println("yes");
    		else
    			System.err.println("no");
    		System.out.println(accesses);
    	} catch (Exception e){
    		System.out.print(e.getMessage());    	}
    }

    /**
     * Create an instance of the Knapsack-N problem.
     *
     * @param candidates the set of data from which a subset will be chosen
     * @param sum the target sum for the data subset
     * @param subsetSize how big the subset must be.
     */
    public KnapsackN( InstrumentedArray candidates, int sum, int subsetSize ) {
        a = candidates;
        targetSum = sum;

        // Initialize the choices array to choose the first N elements
        // of the data array, where N = subsetSize.
        choices = new int[ subsetSize ];
        for ( int i=0; i<subsetSize; ++i ) {
            choices[i] = i;
        }
    }

    /**
     * Compute the next set of choices from the previous. The
     * algorithm tries to increment the index of the final choice
     * first. Should that fail (index goes out of bounds), it
     * tries to increment the next-to-the-last index, and resets
     * the last index to one more than the next-to-the-last.
     * Should this fail the algorithm keeps starting at an earlier
     * choice until it runs off the start of the choice list without
     * Finding a legal set of indices for all the choices.
     *
     * @return true unless all choice sets have been exhausted.
     */
    private boolean nextChoices() {
        for ( int i=choices.length-1; i>=0; --i ) {
            choices[i] += 1;
            for ( int j=i+1; j<choices.length; ++j ) {
                choices[j] = choices[j-1] + 1;
            }
            if ( choices[choices.length-1] < a.size() ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Print out the current set of choices. This method is
     * intended to be for debug purposes.
     */
    private void printChoices() {
        System.out.print( "[" );
        for ( int i=0; i<choices.length; ++i ) {
            System.out.print( " " + choices[i] );
        }
        System.out.println( " ]" );
    }

    /**
     * Run the search algorithm by repeatedly getting a new subset
     * and checking to see if the sum matches. <em>This method
     * runs through all possible subsets, even after it finds one
     * that satisfies the desired sum</em>.
     *
     * @return true if at least one subset yielded the desired sum
     */
    public boolean run() {
        boolean result = false;
        //use a do while loop so it tries the initial choice indices first
        do{
        	if (sum()==targetSum){
        		result=true;
        	}
        }while(nextChoices());
        accesses=a.getTotalReads();
        return result;
    }

    /**
     * Sum the chosen elements.
     * The private array choices indicates the indices in the
     * InstrumentedArray a that are to be added up.
     *
     * @return the sum of the chosen elements.
     */
    private int sum() {
        int result = 0;
        for(int i=0;i<choices.length;i++){
        	result+=a.get(choices[i]);
        }
        return result;
    }

}
