
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
/* 
 * filename.java 
 * 
 * Version: 
 *     $Id: Knapsack.java,v 1.6 2015/09/20 00:35:22 ees8346 Exp $ 
 * 
 * Revisions: 
 *     $Log: Knapsack.java,v $
 *     Revision 1.6  2015/09/20 00:35:22  ees8346
 *     Final
 *
 *     Revision 1.5  2015/09/20 00:21:56  ees8346
 *     Final
 *
 *     Revision 1.4  2015/09/19 23:54:40  ees8346
 *     Final
 * 
 */
/**
 *
 * A solution to the "knapsack" or "subset sum" problem where you are asked if
 * there is a subset of the elements that sum to a desired value.
 *
 * @author Evan
 */
public class Knapsack {

	/**
	 * Read a sum and an array of integers. See if a subset of the array
	 * elements sum to that value.
	 *
	 * @param args
	 *            not used
	 */
	public static void main( String[] args ) {
/**************************************************************
YOUR CODE GOES HERE.
There is an error message that you must print when needed:

"Usage: java Knapsack"
        -- if any command line arguments were provided

**************************************************************/
		if (args.length==0){
			//scanner that will eventually read a data file, because arguments weren't allowed
			Scanner scanner = new Scanner(System.in);
			//ignores first integer since I intend to use the same tester files and the first is used by KnapsackN for subset size
			scanner.nextInt();
			int K = scanner.nextInt();
			List<Integer> list = new LinkedList<Integer>(); 
			while(scanner.hasNext()){
				list.add(scanner.nextInt());
			}
			scanner.close();
			Integer[] data = new Integer[list.size()];
			Iterator<Integer> grab = list.iterator();
			int count=0;
			while (grab.hasNext()){
				data[count]=grab.next();
				count++;
			}
			boolean result=false;
			InstrumentedArray set = new InstrumentedArray(data);
			for (int N=1;N<=set.size();N++){
				KnapsackN sack = new KnapsackN(set,K,N);
				if (sack.run())
					result=true;
			}
			if (result)
				System.err.println("yes");
			else
				System.err.println("no");
			System.out.println(set.getTotalReads());
		}
		/* Wanted to avoid using throw as we couldn't make our own exception class because of try
		 * and I wanted to see the errors messages java gave me
		 */
		else{
			System.out.print("Usage: java Knapsack");
		}
	}
}
