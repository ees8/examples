Language: Java

What it is: A program that solves the Knapsack problem. Where given a set of items, each with a weight and a value, you must
determine the number of each item to include in a collection so that the total weight is 
less than or equal to a given limit and the total value is as large as possible. Solves two versions of it.

Reason for including: Shows work on an advance mathematical solution/

How to use: Takes txt input files through the command line. Several are included.
e.g. Knapsack < input1